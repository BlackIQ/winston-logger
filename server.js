import logger from "./src/log/index.js";
import app from "./src/index.js";

import { AppConnection } from "./src/connections/index.js";

import dotenv from "dotenv";
dotenv.config();

const env = process.env;

const port = env.PORT;

AppConnection.on("open", () => {
    app.listen(
        port,
        () => {
            const ctx = {
                port,
            };

            logger.child({ ctx }).info(`App started, ${port}`);
        }
    );
});