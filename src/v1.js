import express from "express";

const app = express();

import LogRoutes from "./routes/log.routes.js";

app.use("/log", LogRoutes);

export default app;