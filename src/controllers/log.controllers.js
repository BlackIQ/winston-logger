import logger from "../log/index.js";

export const ErrorLog = (req, res) => {
    logger.error("Error");

    res.status(200).send({ message: "Error Log" });
}

export const WarnLog = (req, res) => {
    logger.warn("Warn");

    res.status(200).send({ message: "Warn Log" });
}

export const InfoLog = (req, res) => {
    logger.info("Info");

    res.status(200).send({ message: "Info Log" });
}