import express from "express";
import { ErrorLog, InfoLog, WarnLog } from "../controllers/log.controllers.js";

const router = express.Router();

router.get("/error", ErrorLog);
router.get("/warn", WarnLog);
router.get("/info", InfoLog);

export default router;