import logger from "../../log/index.js";

import mongoose from "mongoose";

import dotenv from "dotenv";
dotenv.config();

const env = process.env;

const url = env.MONGO_LOG;

const connection = mongoose.createConnection(url, (error) => {
    if (error) {
        const ctx = {
            mongo_url: url,
            error: error.message,
        };

        logger.child({ ctx }).error("Can not connect to logger system MongoDB");
    } else {
        const ctx = {
            mongo_url: url,
        };

        logger.child({ ctx }).info("App connected to logger MongoDB")
    }
});

export default connection;