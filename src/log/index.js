import {
    createLogger,
    format,
    transports,
    addColors,
} from "winston";

import {
    MongoDB,
} from "winston-mongodb";

import { LogConnection } from "../connections/index.js"

import dotenv from "dotenv";
dotenv.config();

const env = process.env;

const url = env.MONGO_LOG;

var today = new Date();

var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy + '-' + mm + '-' + dd;

const formatLevels = {
    levels: {
        error: 1,
        warn: 2,
        info: 3,
    },
    colors: {
        error: "red",
        warn: "yellow",
        info: "blue",
    },
};

addColors(formatLevels.colors);

const logger = createLogger({
    levels: formatLevels.levels,
    exitOnError: false,
    transports: [
        new transports.Console({
            format: format.combine(
                format.colorize(),
                format.simple(),
            ),
        }),
        new transports.File({
            filename: `./logs/ticketing-${today}.log`,
            format: format.combine(
                format.timestamp(),
                format.prettyPrint(),
            ), 
        }),
    ],
});

LogConnection.on("open", () => {
    logger.add(
        new transports.MongoDB({
            db: url,
            collection: "Tickecting Logs",
            format: format.combine(
                format.timestamp(),
                format.metadata(), 
            ),
            options: {
                useUnifiedTopology: true,
            },
        })
    );
});

export default logger;